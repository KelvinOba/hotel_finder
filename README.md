This is a mini hotel locator web app

How to make it run locally

clone repo

run 'npm install' in your terminal to install all dependencies

Get a google map Api key

'process.env.VUE_APP_MY_ENV_API_KEY' variable found in the public/index.html, findHotel.vue files should be substituted
for a working google map apikey in order for google libraries to work

then run 'npm run serve' in your terminal to load project